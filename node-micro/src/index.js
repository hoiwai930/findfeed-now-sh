const cors = require('micro-cors')()
const fetch = require('node-fetch')

const handler = async (req, res) => {
  x = require('url').parse(req.url, true).query.x;
  const request = await fetch("https://cloud.feedly.com/v3/search/feeds?query=" + x)
  const data = await request.json()

  return data.results[0].feedId.substring(5);
}

module.exports = cors(handler)

